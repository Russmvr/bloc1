import 'dart:async';
import 'package:razyeb_bloc/bloc_event.dart';

class Bloc{

  Bloc() {
    _eventController.stream.listen((event) {
      _stateController.sink.addStream(_mapEventToState(event));
    });
  }

//  Bloc(){
//    _eventController.stream.listen(_mapEventToState);
//  }
//

  final _eventController = StreamController<BlocMainEvent>.broadcast();
  final _stateController = StreamController<String>.broadcast();

  Stream<String> get stateStream => _stateController.stream;

  Function(BlocMainEvent) get addEvent => _eventController.sink.add;


  Stream <String> _mapEventToState(BlocMainEvent event) async*{ //1
    if (event is BlocMainEvent){
      final String _email = '${event.text}.ru';
      yield _email;
    }
  }



  void dispose(){
    _eventController.close();
    _stateController.close();
  }


}
