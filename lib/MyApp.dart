import 'dart:async';

import 'package:flutter/material.dart';
import 'package:razyeb_bloc/bloc_event.dart';

import 'bloc_state.dart';



class MyApp1 extends StatefulWidget {
  @override
  _MyApp1State createState() => _MyApp1State();
}

class _MyApp1State extends State<MyApp1> {
  Bloc bloc = Bloc();
  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Введи нормальный email')),
      ),
      body: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: emailField(bloc),
              ),
              SizedBox(height: 20),
              RaisedButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('SAVE'),
                onPressed: () {
                  if (!_formKey.currentState.validate()) {
                    return;
                  }
                  _formKey.currentState.save();
                  Navigator.pushNamed(context, '/goto');
                },
              ),
            ],
          )),
    );
  }
}

Widget emailField(Bloc bloc) {
      return TextFormField(
        validator: (String value) {
          if (value.contains('@') != true) {
            return 'NO-NO-NO';
          }
        },
        onSaved: (String value) {
          bloc.addEvent(BlocMainEvent(text: value));
        },
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(labelText: 'E-MAIL'),
      );
    }

