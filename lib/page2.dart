import 'package:flutter/material.dart';

import 'bloc_state.dart';

class Page2 extends StatefulWidget {
  @override
  _Page2State createState() => _Page2State();
}

class _Page2State extends State<Page2> {
  Bloc bloc = Bloc();
  /// StreamBuilder<AbstractState> {
  //stream: ...,
  //builder: (...) {
  //if (snapshot.hasData) {
  //final state = snapshot.data;
  //if (state is State1) {
  //return Text(snapshot.data.text);
  //}
  //if (state is State2) {
  ////...
  //}
  ////...
  //}
  //return Text('hui');},
  //}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('PAGE 2')),
        body: StreamBuilder<String>(
            stream: bloc.stateStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final state = snapshot.data;
              } else {
                return Center(
                  child: Text(
                    'hui',
                    style: TextStyle(fontSize: 30),
                  ),
                );
              }

              return Container(
                child: Center(
                    child: Text(
                  '${snapshot.data}',
                  style: TextStyle(fontSize: 30),
                )),
              );
            }));
  }
}
//bloc.changes;
//await Future.delayed(Duration(milliseconds: 500));
//subscription.cancel;
